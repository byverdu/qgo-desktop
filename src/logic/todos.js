export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const DELETE_ITEM = 'qgo/assessment/DELETE_ITEM';
export const COMPLETE_ITEM = 'qgo/assessment/COMPLETE_ITEM';

export const addItem = (content) => {
  return { type: ADD_ITEM, content };
};
export const deleteItem = (position) => {
  return { type: DELETE_ITEM, position };
};
export const completeItem = (position, itemStatus) => {
  return { type: COMPLETE_ITEM, position, itemStatus };
};

export const initialState = {
  items: [
    { id: 1, content: 'Call mum', completed: false },
    { id: 2, content: 'Buy cat food', completed: false },
    { id: 3, content: 'Water the plants', completed: false },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };
    
    case DELETE_ITEM:
      const filteredItems = state.items.filter((item, index) => index !== action.position);

      return {
        ...state,
        items: filteredItems
      }

    case COMPLETE_ITEM:
      state.items[action.position].completed = action.itemStatus;
      return {
        ...state
      }
    default:
      return state;
  }
};

export default reducer;
