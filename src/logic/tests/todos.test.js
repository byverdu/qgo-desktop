import reducer, { initialState, addItem, deleteItem, completeItem } from '../todos';

const state = {
  items: [
    { id: 1, content: 'first', completed: false},
    { id: 2, content: 'second', completed: false}
  ],
};

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const mockAction = addItem('third');
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });
  it('should delete items on DELETE_ITEM', () => {
    const mockAction = deleteItem(1);
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
    expect(result.items[0].id).toEqual(1);
    expect(result.items[0].content).toEqual('first');
  });
  it('should toggle items state on COMPLETE_ITEM', () => {
    const mockTruthyAction = completeItem(0, true);
    const resultTruthy = reducer(state, mockTruthyAction);
    expect(resultTruthy.items[0].completed).toBeTruthy();
  });
  it('should toggle items state on COMPLETE_ITEM', () => {
    completeItem(0, true);
    const mockFalsyAction = completeItem(0, false);
    const resultFalsy = reducer(state, mockFalsyAction);
    expect(resultFalsy.items[0].completed).toBeFalsy();
  });
});
