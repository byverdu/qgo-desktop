import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { deleteItem, completeItem } from '../../logic/todos';
import './styles.css';

const paragraphRenderer = () => <p id="items-missing">Add some tasks above.</p>;
export class ItemsList extends React.Component {
  constructor( props ) {
    super(props);

    this.state = {
      isCompleted: false,
      toggleCompleted: false
    };

    this.handleToogleCompleted = this.handleToogleCompleted.bind(this);
  }

  handleToogleCompleted(event) {
    this.setState({
      toggleCompleted: event.target.checked
    })
  }

  itemsListRenderer(data) {
    const {
      items
    } = this.props
    const tempItems = this.state.toggleCompleted ? 
      items.filter(item => !item.completed) :
      items;
    return (
      tempItems.map((item, index) => <li
        key={item.id}
        className={classnames({ 'isCompleted': item.completed })}
      >
        <label>
          <input
              onChange={() => {
                this.setState({
                  isCompleted: !item.completed
                });
                this.props.onComplete(index, !item.completed);
              }}
              type="checkbox"
            />
          {item.content}
        </label>
        <button
          onClick={() => this.props.onDelete(index)}
        >
          Delete
      </button>
      </li>
    )
  )} 

  render() {
    return(
      <div>
        {
          this.props.items.length > 0 && 
          <label>
            <input
                onChange={(event) => {
                  this.handleToogleCompleted(event);
                }}
                type="checkbox"
              />
            Hide Completed Tasks
          </label>
        }
         <ul className="itemsList-ul">
           {this.props.items.length < 1 && paragraphRenderer()}
           {this.itemsListRenderer(this.props.items)}
         </ul>
       </div>
    )
  }
}

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return { items: state.todos.items };
};

const mapDispatchToProps = (dispatch) => ({
  onDelete: (position) => dispatch(deleteItem(position)),
  onComplete: (position, itemStatus) => dispatch(completeItem(position, itemStatus)),
});

export default connect(
  mapStateToProps, mapDispatchToProps
)(ItemsList);
