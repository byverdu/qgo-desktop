![Sky logo](./docs/assets/logo.png)

# QGO Desktop Assessment

## Overview

Thanks for your interest a development role on the QGO Desktop team!

We'd like you to complete this short technical test before we bring you in for
an in person interview. It's a chance to show off your skills and show us how
you approach development.

This technical test is written in React and uses Redux for state management.
It assumes familiarity with React and Redux. If you aren't familiar with these
then let us know before starting the test!

You'll be working on a simple To Do app, extending it with a few new
features.

## Timing

About 90 mins.

If you don't get everything done in 90 mins don't worry, just send across what
you have with a note about what is left to do. We know your time is precious
and you don't want to spend all day on a technical test!

Given the time constrain we'd rather you focused on:
* Making it work, first and foremost!
* Unit tests.

Don't worry about making it look nice, or fancy CSS effects. We're happy with
unstyled buttons and elements. We'll be focusing on the code more than the
presentation.

## Getting Started

Install the required dependencies using npm:

```
npm install
```

You can then start the app with:

```
npm start
```

And run the tests (in watch mode) with:

```
npm test
```

## Tasks

We'd like you to make the following changes to the To Do app:

1. Add the ability to delete items.
2. Be able to mark items as complete. And then toggle them back to incomplete.
3. Add a filter than can be toggled to hide completed items.

## Finishing up

When you are finished with the test please send it back to us (either a link to
the repo, or a zip of the project).

It would be great if you could write up a short summary of how you approached the task, and if there are any improvements you would have made if given more time.

## Notes

Ideally this should feel as much like normal development as possible.

* Feel free to search for any information you need.
* You can add any libraries you need to get the job done.
* Please contact us if you have any questions about the task, we're happy to
  give more details!

### Summary about the assesment

The first thing that I have done is to understand how the application was structured and what coding convections were used.

For the implementation for the two first tasks I started by adding some unit tests for the reducer and once that was done modify the actual component (ItemsList).

When doing the toggle task I realized that the component should have some state in order to re-render after some of the actions were dispatched so I refactored the `ItemsList` component from a stateless component to a one that was extending the React Component.

For the last task because it did not needed any change on the redux store I implemented working directly in the component.

To do the assesment took me around 30 minutes more than what has been agreed mainly because when I was doing the last task (toggle completed tasks) I have realized that everything stopped working and that was something that I had to resolve. That reinforce the idea that having your code tested is less prone to errors.

#### What I would have improved (by importance order)
  - Analyse better what was needed and how to approach the tasks.
  - Create some extra components like Task, Button or Input for example so the `ItemsList` class could be more smaller.
  - Better naming for the variables.
  - Move redux actions to own folder
  - Add more enzyme tests to check user interaction.
  - Add styles.